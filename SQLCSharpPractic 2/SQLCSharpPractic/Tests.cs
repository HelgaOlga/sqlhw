﻿using System.Configuration;
using System.Data;
using NUnit.Framework;

namespace SqlTests
{
    [TestFixture]
    public class Tests
    {
        private readonly SqlDataReader _sqlDataReader;

        public Tests()
        {
            _sqlDataReader = new SqlDataReader(); //инициализация sqlDataReader
        }

        [Test]
        public void PersonCount_Test()
        {
            DataTable persons = _sqlDataReader.GetPersonsOlder30();
            Assert.AreEqual(4, persons.Rows.Count);
        }

        [Test]
        public void TotalSum_Test()
        {
            DataTable persons = _sqlDataReader.GetTotalOrder();
            Assert.AreEqual(1, persons.Rows.Count); //проверка что вернуло 1 строку
            Assert.IsTrue(persons.Rows[0]["sum"].ToString() == "152536"); //проверка на общую сумму заказов
        }

        [Test]
        public void PersonsWithoutOrders_Test()
        {
            DataTable persons = _sqlDataReader.GetPersonsWithoutOrders();
            Assert.AreEqual(9, persons.Rows.Count);
        }


    }
}
