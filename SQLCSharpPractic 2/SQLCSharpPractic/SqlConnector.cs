﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SqlTests
{
    public  class SqlConnector
    {
        private static string _connectionString;

        /// <summary>
        /// заполнения строки подключения connectionString
        /// </summary>
        /// <param name="catalogName"></param>
        public static void ConnectToCatalog(string catalogName)
        {
            _connectionString = $"Data Source={ConfigurationManager.AppSettings["serverName"]}; " +
                                $"Initial Catalog= {catalogName}; Integrated Security=True; ";
        }
        /// <summary>
        ///выполнение sql команды
        /// </summary>
        /// <param name="sqlRequest"></param>
        /// <returns></returns>
        public static DataTable Execute(string sqlRequest)
        {
            SqlConnector.ConnectToCatalog(ConfigurationManager.AppSettings["databaseName"]);
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;

                    var adapter = new SqlDataAdapter(sqlCommand);

                    adapter.Fill(dataSet);
                }
            }
            return dataSet.Tables[0];

        }
          
    }
}
