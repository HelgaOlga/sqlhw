﻿using System.Data;

namespace SqlTests
{
    class SqlDataReader
    {
        /// <summary>
        /// получение пользователей старше 30лет
        /// </summary>
        /// <returns></returns>
        public DataTable GetPersonsOlder30()
        {
            return SqlConnector.Execute("SELECT FirstName, LastName, Age FROM Person WHERE Age > 30");
        }

        /// <summary>
        /// получение общей суммы всех заказов
        /// </summary>
        /// <returns></returns>
        public DataTable GetTotalOrder()
        {
            return SqlConnector.Execute("SELECT SUM(Total) AS sum FROM orders");
        }

        /// <summary>
        /// пользователи без заказа
        /// </summary>
        /// <returns></returns>
        public DataTable GetPersonsWithoutOrders()
        {
            DataTable persons = SqlConnector.Execute
            (@"SELECT p.FirstName, p.LastName, p.Age 
                                FROM Person as p LEFT JOIN Orders AS o 
                                ON p.ID = o.PERSON_ID 
                                WHERE o.total IS NULL");
            return persons;
        }
    }
}
